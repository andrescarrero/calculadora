#calculadora basica en python  
class calculadora:
    #metodo constructor de la clase
    def __init__(self):
        self.operando_1=0
        self.operando_2=0
        self.resultado=0

    def sumar(self):
        self.resultado= self.operando_1 + self.operando_2

    def restar(self):
        self.resultado= self.operando_1 - self.operando_2

    def multiplicar(self):
        self.resultado= self.operando_1 * self.operando_2

    def dividir(self):
        #to do evitar divicion por cero 
        self.resultado= self.operando_1 / self.operando_2

    def modulo(self):
        self.resultado= self.operando_1 % self.operando_2